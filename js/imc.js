const btnCalcular = document.getElementById('btnCalcular');

btnCalcular.addEventListener("click", function(){

    let valorPeso = document.getElementById('peso').value;
    let valorAltura = document.getElementById('altura').value;

    let valorIMC = valorPeso/(valorAltura * valorAltura);

    document.getElementById('imc').value = valorIMC;
})